# INSERT INTO users(id, firstname, lastname, username,email, password)
# VALUES(null, 'Donovan', 'Mitchell', 'dmitchell', 'mitchell@gmail.com','123456')
#      ,(null, 'Rudy', 'Gobert', 'rgobert', 'gobert@gmail.com','123456')
#      ,(null, 'Ricky', 'Rubio', 'rrubio', 'rubio@gmail.com','123456')
#      ,(null, 'Joe', 'Ingles', 'jingles','ingles@gmail.com','123456')
#      ,(null, 'Jae', 'Crowder', 'jcrowder','crowder@gmail.com','123456');


INSERT INTO event_model(id, name, type , starttime,endtime, address, city,state,zipcode, created_by)
VALUES(null, 'party', 10, '2019-07-07 18:00:00', '2019-07-07 22:00:00','road','Layton','Utah','84040',1)
     ,(null, 'party', 10, '2019-07-07 18:00:00', '2019-07-07 22:00:00','road','Layton','Utah','84040',1)
     ,(null, 'party', 10, '2019-07-07 18:00:00', '2019-07-07 22:00:00','road','Layton','Utah','84040',1)
     ,(null, 'party', 10, '2019-07-07 18:00:00', '2019-07-07 22:00:00','road','Layton','Utah','84040',1)
     ,(null, 'party', 10, '2019-07-07 18:00:00', '2019-07-07 22:00:00','road','Layton','Utah','84040',1)
     ,(null, 'Super Smash Tournament', 1, '2019-07-04 18:00:00', '2019-07-07 22:00:00','56 Cleveland Lane','Herndon','VA','20170', 1)
     ,(null, 'Arkham Horror Night', 2, '2019-12-05 19:00:00', '2019-12-05 23:00:00','8083 Rockville Rd.','Bay City','MI','20170', 1)
     ,(null, 'Superb Owl Football Night', 3, '2019-02-03 14:00:00', '2019-02-03 21:00:00','160 W. Lantern Rd.','Villa Park','IL','60181', 1)
     ,(null, 'Out of Town Parents Pool Party', 4, '2019-06-06 16:00:00', '2019-06-06 01:00:00','480 Manor Dr.','Garland','TX','75043', 1)
     ,(null, 'Death Cab for Cutie', 5, '2019-05-12 16:00:00', '2019-06-06 01:00:00','748 S W Kilby Ct','Salt Lake City','UT','84101', 1)
     ,(null, 'Salt Lake Comic Con', 6, '2019-08-08 10:00:00', '2019-08-12 01:00:00','100 S W Temple','Salt Lake City','UT','84101', 1)
     ,(null, 'Farewell Party at Beer Bar', 7, '2019-05-12 18:00:00', '2019-05-12 01:00:00','161 E 200 S','Salt Lake City','UT','84111', 1)
     ,(null, 'Birthday Dinner for Jo', 8, '2019-12-05 18:00:00', '2019-12-05 20:00:00','161 E 200 S','Salt Lake City','UT','84111', 1)
     ,(null, 'Northridge High Reunion Class of 1999', 9, '2019-12-05 18:00:00', '2019-12-05 20:00:00','2430 N Hill Field Rd','Layton','UT','84041', 1)
       ,(null, 'Fundraiser for the Pantry', 10, '2019-12-02 10:00:00', '2019-12-02 13:00:00','475 Garfield Drive','Barrington','IL','60010', 1);



INSERT INTO type_model(id,name,image)
VALUES(null, 'Gaming', '../../assets/images/gaming-small.jpg')
     ,(null, 'Board Games', '../../assets/images/game-night-small.jpg')
     ,(null, 'Sports', '../../assets/images/sports-small.jpg')
     ,(null, 'Pool', '../../assets/images/pool-party-small.jpg')
     ,(null, 'Concert', '../../assets/images/concert-small.jpg')
     ,(null, 'Cosplay', '../../assets/images/cosplay-small.jpg')
     ,(null, 'Bar', '../../assets/images/bar-small.jpg')
     ,(null, 'Birthday', '../../assets/images/birthday-small.jpg')
     ,(null, 'Reunion', '../../assets/images/reunion-small.jpg')
     ,(null, 'Other', '../../assets/images/other-small.jpg');

INSERT INTO attendees_model(id,event, user)
VALUES(null, 1, 1)
     ,(null, 2,2)
     ,(null, 3, 3)
     ,(null, 4, 4)
     ,(null, 5,5);

INSERT INTO roles(id, name)
VALUES (null, 'ROLE_USER')
      ,(null, 'ROLE_PM')
      ,(null,'ROLE_ADMIN');

INSERT INTO user_roles(user_id, role_id)
VALUES (1,1)
      ,(2,1)
      ,(3,1)
      ,(4,1)
      ,(5,1)
