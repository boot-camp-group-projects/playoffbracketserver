package com.party.partydate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PartydateserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(PartydateserverApplication.class, args);
    }

}
