package com.party.partydate.controller;

import com.party.partydate.model.AttendeesModel;
import com.party.partydate.model.EventModel;
import com.party.partydate.model.TypeModel;
import com.party.partydate.repository.AttendeesRepository;
import com.party.partydate.repository.EventRepository;
import com.party.partydate.repository.TypeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class EventController {

    private EventRepository eventRepository;
    private TypeRepository typeRepository;
    private AttendeesRepository attendeesRepository;

    public EventController(EventRepository eventRepository, TypeRepository typeRepository, AttendeesRepository attendeesRepository) {
        this.eventRepository = eventRepository;
        this.typeRepository = typeRepository;
        this.attendeesRepository = attendeesRepository;
    }

    @GetMapping("/events")
    public List<EventModel> getAllEvents() { return (List<EventModel>) eventRepository.findAll(); }

    @GetMapping("/events/{id}")
    public List<EventModel> getEventById(@PathVariable Long id) { return eventRepository.findEventModelById(id); }

    @PostMapping("/events/create")
    public void createEvents(@RequestBody EventModel e) {
        eventRepository.save(e);
    }

    @GetMapping("/types")
    public List<TypeModel> getAllTypes() { return (List<TypeModel>) typeRepository.findAll(); }

    @GetMapping("/myevents/{name}")
    public List<AttendeesModel> getEventByUserId(@PathVariable String name) { return attendeesRepository.findAttendeesModelByUserUsername(name);}

}

