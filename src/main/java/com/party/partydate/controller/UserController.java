package com.party.partydate.controller;

import com.party.partydate.model.User;
import com.party.partydate.repository.CustomerRepository;
import com.party.partydate.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RepositoryRestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private UserRepository userRepository;
    public UserController(CustomerRepository customerRepo){this.userRepository = userRepository;}

    @GetMapping(value = "/customers")
    public List<User> getAllCustomers() {
        return (List<User>) userRepository.findAll();
    }

    @GetMapping("/user/{name}")
    public Long getUserByName(@PathVariable String name) {
        Optional<User> userId = customerRepo.findByUsername(name);
        Long usrId = userId.get().getId();
        return usrId;
    }

//    @GetMapping("/customers/{id}")
//    public User getCategoryById(@PathVariable Long id) {return this.userRepository.findAllById(id);}

    @PostMapping("/customers/create")
    public User createStudent(@RequestBody User creatme) {

        customerRepo.save(creatme);
        return creatme;
    }
}
