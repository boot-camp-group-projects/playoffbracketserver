package com.party.partydate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Entity
public class EventModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "starttime")
    private Date starttime;
    @Column(name = "endtime")
    private Date endtime;
    @Column(name = "address")
    private String address;
    @Column(name = "city")
    private String city;
    @Column(name = "state")
    private String state;

    @NotBlank
    @Size(min = 5, max = 5)
    @Column(name = "zipcode")
    private String zipcode;

    @ManyToOne
    @JoinColumn(name = "CreatedBy", referencedColumnName = "ID")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "ID")
    private TypeModel type;

}
