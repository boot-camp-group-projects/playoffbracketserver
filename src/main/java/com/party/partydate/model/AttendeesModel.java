package com.party.partydate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class AttendeesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "event", referencedColumnName = "ID")
    private EventModel event;

    @ManyToOne
    @JoinColumn(name = "user", referencedColumnName = "ID")
    private User user;

}
