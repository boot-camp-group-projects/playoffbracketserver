package com.party.partydate.repository;

import com.party.partydate.model.TypeModel;
import org.springframework.data.repository.CrudRepository;

public interface TypeRepository extends CrudRepository<TypeModel,Long> {
}
