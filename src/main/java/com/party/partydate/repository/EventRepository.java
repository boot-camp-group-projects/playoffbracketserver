package com.party.partydate.repository;

import com.party.partydate.model.EventModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventRepository extends CrudRepository<EventModel, Long> {

    List<EventModel> findEventModelById(Long id);
}
