package com.party.partydate.repository;

import com.party.partydate.model.AttendeesModel;
import com.party.partydate.model.EventModel;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AttendeesRepository extends CrudRepository<AttendeesModel, Long> {
    List<AttendeesModel> findAllByUser_Id(Long id);
    List<AttendeesModel> findAttendeesModelByUserUsername(String name);

}
